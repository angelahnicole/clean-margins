import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/*******************************************************
 * cleanMargins.java
 * 
 * Angela Gross
 *
 * This class reads in a file of text, and from that is 
 * able to print the paragraph of text on a screen that
 * can only handle L characters per line (as specified
 * in the Driver). It makes use of dynamic programming 
 * instead of using a greedy algorithm to print text in
 * a "visually appealing" manner, or in a way that 
 * minimizes sloppiness, where sloppiness is defined as
 * the cube of the leftover spaces at the end of a line.
 * 
 *******************************************************/


public class cleanMargins 
{
	//////////////////////////////////////////////////////////////////
	
	// declaring ATTRIBUTES
	
	ArrayList<String> Words = new ArrayList<String>();
	int[] bestMargins;
	String[] bestOutput;
	
	File myFile;
	
	final int Infinity = Integer.MAX_VALUE;
	int lineLength;
	int numWords;
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTOR
	public cleanMargins(File newFile)
	{
		myFile = newFile;
	}
	
	//////////////////////////////////////////////////////////////////
	
	// --------------------------------------------------------------
	// readFile METHOD
	// --------------------------------------------------------------
	// This method only supports the following format:
	//
	// Integer
	// Block of text
	// 
	// The block of text can contain line breaks, but remember that 
	// this program will treat the text as one paragraph, and will
	// format it as such. Also, if you format the text file any other
	// way, it may break. So don't do it.
	// 
	// Also, this method also initializes some data structures and
	// retrieves how many words we have. (Since everything is 1 
	// indexed, we have a dummy value at Words[0]- so the amount
	// of words is Words.size()-1). 
	// --------------------------------------------------------------
	public void readFile(File myFile) throws IOException
	{
		Scanner myScan = new Scanner(myFile);
		String Paragraph = "";
		String lineTest = "";
		String Word = "";
		int i = 1;
		lineLength = 0;
		
		lineLength = myScan.nextInt();
		
		while(myScan.hasNext())
		{
			Paragraph = Paragraph + myScan.nextLine() + " ";
		}
		
		myScan = new Scanner(Paragraph);
		myScan.useDelimiter("[ ]+");
		
		Words.add(0, "");
		
		while(myScan.hasNext())
		{
			Word = myScan.next();
			Words.add(i, Word);
			i++;
		}
		
		numWords = Words.size() - 1;
		bestMargins = new int[numWords+1];
		bestOutput = new String[numWords+1];
		
		for(int j = 0; j <= numWords; j++)
			bestOutput[j] = "";
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ---------------------------------------------------------------
	// formatText() METHOD i.e. find best[k]
	// ---------------------------------------------------------------
	// This method makes use of the following recurrence:
	// best[k] = min { best[i-1] + lineCost[i, k] } for 1 <= i <= k,
	// where best[k] is the optimal cost to print words 1 through k 
	// when you are limited to L characters per line, and 
	// lineCost[k, i] is the amount of sloppiness to print words 
	// k through i on the screen. There's more on lineCost[k,i] below.
	//
	// This simply implements the recurrence and keeps track of the
	// output. Keep in mind that everything is 1 indexed in order
	// to have a direct relation to the recurrence.
	//
	// If the recurrence is Greek to you, then consult 
	// Project_4_WriteUp.pdf.
	// ---------------------------------------------------------------
	public String formatText()
	{
		String currentOutput = "";
		int leastSloppy, currentSloppiness;
		
		bestMargins[0] = 0;
		
		for(int k = 1; k <= numWords; k++)
		{
			leastSloppy = Infinity;
			
			for(int i = k; i > 0; i--)
			{
				currentSloppiness = findSloppiness(i, k);
				
				if ((currentSloppiness != Infinity) 
						&& (bestMargins[i-1] + currentSloppiness  < leastSloppy))
				{
					leastSloppy = bestMargins[i-1] + currentSloppiness;
					currentOutput = bestOutput[i-1] + getWords(i, k);
				}
			}
			
			bestMargins[k] = leastSloppy;
			bestOutput[k] = currentOutput + "\n";
		}
		
		return bestOutput[numWords];
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ---------------------------------------------------------------
	// spacesLeft() and findSloppiness() METHODS i.e. lineCost[i, j]
	// ---------------------------------------------------------------
	// These methods find how much it costs to store words i through 
	// j on a single line. Instead of simply returning L - j + i - sum
	// cubed to my formatText() method, (where sum is the length of all 
	// the words and L is the desired line length), I check that the  
	// spaces left on the line aren't negative, and if they are, then
	// I'll return "Infinity", (i.e. the largest number Java can 
	// represent in Integer form), as we're looking for the smallest
	// amount of sloppiness and we must not consider the line 
	// configurations in which the amount of characters exceed the
	// line length requirement.
	// 
	// Thus,
	// lineCost[i, j] = { (spacesLeft)^3 if spacesLeft >= 0
	// 						Infinity if spacesLeft < 0 }
	// ---------------------------------------------------------------
	
	public int spacesLeft(int i, int j) throws IllegalArgumentException
	{
		int sum = 0;

		for(int k = i; k <= j; k++)
		{
			if(Words.get(k).length() > lineLength)
			{
				throw new IllegalArgumentException
				("Error: L cannot be less than any given word length");
			}
			
			sum = sum + Words.get(k).length();
		}

		return lineLength - j + i - sum;
	}
	
	public int findSloppiness(int i, int j)
	{
		int spacesLeft = spacesLeft(i, j);
		
		if(spacesLeft < 0)
			return Infinity;
		else
			return spacesLeft*spacesLeft*spacesLeft;
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ---------------------------------------------------------------
	// getWords() METHOD
	// ---------------------------------------------------------------
	// This method simply returns words i through j with spaces 
	// between them- nothing fancy at all.
	// ---------------------------------------------------------------
	public String getWords(int i, int j)
	{
		String desiredWords = "";
		for(int k = i; k <= j; k++)
		{
			desiredWords = desiredWords + Words.get(k) + " ";
		}
		
		return desiredWords;
	}
	
	//////////////////////////////////////////////////////////////////

}
