import java.io.File;

/*******************************************************
 * cleanMarginsDriver.java
 * 
 * Angela Gross
 * 
 * --------------------------------------------------
 * Project 4 Specifications:
 * --------------------------------------------------
 * You are given a sequence of words which comprise a 
 * paragraph of text. You need to print the paragraph 
 * (in a fixed-width font) on a screen that can only 
 * handle L characters per line. In this problem, 
 * you are interested in breaking the paragraph into 
 * lines while making the layout visually appealing. 
 * The sloppiness of a line is defined as the cube 
 * of the number of spaces at the end of the line. 
 * n this problem, you will read from an input file 
 * proj4.dat. The file consists of one line containing 
 * the integer L, followed by a list of words to 
 * be formatted (separated by whitespace, possibly 
 * including line breaks). You must output these words 
 * in a paragraph, with each line containing at most L 
 * characters, that minimizes the total sloppiness value. 
 * 
 * Fun-Fact: I used this program to format the above
 * text! You can find it in proj4-4.dat :)
 *  
 *******************************************************/

public class cleanMarginsDriver 
{

	public static void main(String[] args) 
	{
		//////////////////////////////////////////////////////////////////
		
		File myFile = new File("proj4.dat");
		cleanMargins marginCleaner = new cleanMargins(myFile);
		
		try 
		{
			marginCleaner.readFile(myFile);
		} 
		catch (Exception IOException) 
		{
			System.out.println("Error: Bad File.");
		}
		
		System.out.println(marginCleaner.formatText());
		
		//////////////////////////////////////////////////////////////////

	}

}
